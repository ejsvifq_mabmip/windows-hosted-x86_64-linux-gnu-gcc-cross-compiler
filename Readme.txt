This is a GCC crossback toolchain (multilib). Built by the fast_io library's author

Unix Timestamp:1675641932.004774139
UTC:2023-02-06T00:05:32.004774139Z

fast_io:
https://github.com/cppfastio/fast_io

build	:	x86_64-linux-gnu
host	:	x86_64-w64-mingw32
target	:	x86_64-linux-gnu

x86_64-pc-linux-gnu-g++ -v
Using built-in specs.
COLLECT_GCC=x86_64-pc-linux-gnu-g++
COLLECT_LTO_WRAPPER=d:/toolchains/gnu/x86_64-w64-mingw32/x86_64-pc-linux-gnu/bin/../libexec/gcc/x86_64-pc-linux-gnu/13.0.1/lto-wrapper.exe
Target: x86_64-pc-linux-gnu
Configured with: /home/cqwrteur/toolchains_build/gcc/configure --disable-nls --disable-werror --enable-languages=c,c++ --enable-multilib --with-multilib-list=m64,m32,mx32 --with-gxx-libcxx-include-dir=/home/cqwrteur/toolchains/x86_64-w64-mingw32/x86_64-pc-linux-gnu/x86_64-pc-linux-gnu/include/c++/v1 --prefix=/home/cqwrteur/toolchains/x86_64-w64-mingw32/x86_64-pc-linux-gnu --build=x86_64-pc-linux-gnu --host=x86_64-w64-mingw32 --target=x86_64-pc-linux-gnu --disable-bootstrap --disable-libstdcxx-verbose --with-libstdcxx-eh-pool-obj-count=0 --enable-libstdcxx-backtrace
Thread model: posix
Supported LTO compression algorithms: zlib
gcc version 13.0.1 20230205 (experimental) (GCC)
